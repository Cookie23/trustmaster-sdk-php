# Trustmaster SDK (PHP)

## Usage
### get trust data
```
<?php
require('vendor/autoload.php');

$clientId = 3;
$clientSecret = '5zA2r0NOfNpb97ihpKSB8CarTPMGbCpfo983LAxA';
$googleId = '110477265576843912947';

$client = new \RESWUE\Trust\Client($clientId, $clientSecret);
$token = $client->fetchClientCredentialsGrant([]);

$trustInformation = $client->getTrustInformationByGoogleId($googleId, $token);

// Now store $token somewhere safe for later use.
// Cache $trustInformation to avoid hammering the API.
```

### oauth login
#### get auth url
```php
<?php
$clientId = 'id';
$clientSecret = 'secret';
$uri = 'http://www.example.com/';
$scopes = ['get-agent-name'];

$client = new \RESWUE\Trust\Client($clientId, $clientSecret);
$client->setRedirectUri($uri);
$client->setScopes($scopes);
$auth_url = $client->createAuthUrl();
echo $auth_url;
```

#### authenticate the user after redirect from Trustmaster
```php
<?php
$clientId = 'id';
$clientSecret = 'secret';
$uri = 'http://www.example.com/';
$scopes = ['get-agent-name'];

$client = new \RESWUE\Trust\Client($clientId, $clientSecret);
$client->setRedirectUri($uri);
$client->setScopes($scopes);
$token = $client->authenticate($_GET['code']);

$profile = $client->me($token);
echo $profile->getGoogleId();
```