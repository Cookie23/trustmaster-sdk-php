<?php

namespace RESWUE\Trust;

class Profile
{
    private $trustmasterId;
    private $googleId;
    private $agentName;
    private $googleAvatar;
    private $avatar;
    private $googleName;
    private $name;
    private $email;
    private $telegramUsername;
    private $telegramId;
    private $locationLat;
    private $locationLng;
    private $locationPrivacy;
    private $zelloUsername;
    //private $rawData;

    public function __construct(array $data)
    {
    	//$this->setRawData($data);
        $this->setTrustmasterId($data['trustmaster_id']);

        if (isset($data['social']['google'][0]['id'])) {
            $this->setGoogleId($data['social']['google'][0]['id']);
        }

        if (isset($data['social']['google'][0]['avatar'])) {
            $this->setGoogleAvatar($data['social']['google'][0]['avatar']);
        }

        if (isset($data['social']['google'][0]['name'])) {
            $this->setGoogleName($data['social']['google'][0]['name']);
        }

        if (isset($data['avatar'])) {
            $this->setAvatar($data['avatar']);
        }

        if (isset($data['name'])) {
            $this->setName($data['name']);
        }

        if (isset($data['email'])) {
            $this->setEmail($data['email']);
        } else if (isset($data['social']['google'][0]['email'])) {
            $this->setEmail($data['social']['google'][0]['email']);
    	}

        if (isset($data['social']['telegram'][0]['username'])) {
            $this->setTelegramUsername($data['social']['telegram'][0]['username']);
        }

        if (isset($data['social']['telegram'][0]['id'])) {
            $this->setTelegramId($data['social']['telegram'][0]['id']);
        }

        if (isset($data['location']['lat'])) {
            $this->setLocationLat($data['location']['lat']);
        }

        if (isset($data['location']['lng'])) {
            $this->setLocationLng($data['location']['lng']);
        }

        if (isset($data['location']['privacy'])) {
            $this->setLocationPrivacy($data['location']['privacy']);
        }

        if (isset($data['agent_name'])) {
            $this->setAgentName($data['agent_name']);
        }

        if (isset($data['social']['zello'][0]['username'])) {
            $this->setZelloUsername($data['social']['zello'][0]['username']);
        }
    }

    /**
     * @return string
     */
    public function getRawData()
    {
        return $this->rawData;
    }

    /**
     * @param string $rawData
     */
    public function setRawData($rawData)
    {
        $this->rawData = $rawData;
    }

    /**
     * @return string
     */
    public function getTrustmasterId()
    {
        return $this->trustmasterId;
    }

    /**
     * @param string $trustmasterID
     */
    public function setTrustmasterId($trustmasterId)
    {
        $this->trustmasterId = $trustmasterId;
    }
    /**
     * @return string
     */
    public function getGoogleId()
    {
        return $this->googleId;
    }

    /**
     * @param string $googleId
     */
    public function setGoogleId($googleId)
    {
        $this->googleId = $googleId;
    }

    /**
     * @return string
     */
    public function getAgentName()
    {
        return $this->agentName;
    }

    /**
     * @param string $agentName
     */
    public function setAgentName($agentName)
    {
        $this->agentName = $agentName;
    }

    /**
     * @return string
     */
    public function getGoogleAvatar()
    {
        return $this->googleAvatar;
    }

    /**
     * @param string $googleAvatar
     */
    public function setGoogleAvatar($googleAvatar)
    {
        $this->googleAvatar = $googleAvatar;
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @return string
     */
    public function getGoogleName()
    {
        return $this->googleName;
    }

    /**
     * @param string $googleName
     */
    public function setGoogleName($googleName)
    {
        $this->googleName = $googleName;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getTelegramUsername()
    {
        return $this->telegramUsername;
    }

    /**
     * @param string $telegramUsername
     */
    public function setTelegramUsername($telegramUsername)
    {
        $this->telegramUsername = $telegramUsername;
    }

    /**
     * @return string
     */
    public function getTelegramId()
    {
        return $this->telegramId;
    }

    /**
     * @param string $telegramId
     */
    public function setTelegramId($telegramId)
    {
        $this->telegramId = $telegramId;
    }

    /**
     * @return string
     */
    public function getLocationLat()
    {
        return $this->locationLat;
    }

    /**
     * @param string $locationLat
     */
    public function setLocationLat($locationLat)
    {
        $this->locationLat = $locationLat;
    }

    /**
     * @return string
     */
    public function getLocationLng()
    {
        return $this->locationLng;
    }

    /**
     * @param string $locationLng
     */
    public function setLocationLng($locationLng)
    {
        $this->locationLng = $locationLng;
    }

    /**
     * @return string
     */
    public function getLocationPrivacy()
    {
        return $this->locationPrivacy;
    }

    /**
     * @param string $locationPrivacy
     */
    public function setLocationPrivacy($locationPrivacy)
    {
        $this->locationPrivacy = $locationPrivacy;
    }

    /**
     * @return string
     */
    public function getZelloUsername()
    {
        return $this->zelloUsername;
    }

    /**
     * @param string $zelloUsername
     */
    public function setZelloUsername($zelloUsername)
    {
        $this->zelloUsername = $zelloUsername;
    }
}
