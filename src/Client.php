<?php

namespace RESWUE\Trust;

use GuzzleHttp\Client as HttpClient;

/**
 * \RESWUE\Trust\Client is a PHP client to interact with RESWUE Trustmaster.
 */
class Client
{
    /**
     * The HTTP Client instance.
     *
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;

    protected $clientId;
    protected $secret;
    protected $url;
    protected $userAgent = 'trustmaster-toolbox';
    protected $redirectUri;
    protected $scopes = [];
    protected $state;

    private $connect_timeout_sec = 1.0;
    private $timeout_sec = 2.0;

    public function __construct($clientId, $secret, $url = 'https://trust.reswue.net')
    {
        $this->clientId = $clientId;
        $this->secret = $secret;
        $this->url = $url;
	$this->state=random_bytes(64);
    }


    /**
     * Fetches a 'client_credentials' OAuth token from Trustmaster.
     *
     * Note: Store this token somewhere safe for later use, in order to
     *       avoid this request.
     *
     * @return  \RESWUE\Trust\AccessToken
     */
    public function fetchClientCredentialsGrant(array $scopes = [])
    {
        $response = $this->getHttpClient()->request(
            'POST',
            $this->url.'/oauth/token',
            [
                'timeout' => $this->timeout_sec,
                'connect_timeout' => $this->connect_timeout_sec,
                'form_params' => [
                    'grant_type'    => 'client_credentials',
                    'client_id'     => $this->clientId,
                    'client_secret' => $this->secret,
                    'scope'         => implode(' ', $scopes),
                ],
                'headers' => [
                    'Accept'        => 'application/json',
                    'User-Agent'    => $this->userAgent,
                ],
            ]
        );
        return new AccessToken(json_decode((string) $response->getBody(), true));
    }

    /**
     * Fetches Trust information for the user identified by the given $googleId.
     * The request is authenticated using the given AccessToken
     *
     * @param   string                      $googleId
     * @param   \RESWUE\Trust\AccessToken   $token
     * @return  mixed
     */
    public function getTrustInformationByGoogleId($googleId, AccessToken $token)
    {
        $response = $this->getHttpClient()->request(
            'GET',
            $this->url.'/api/v4/user/google:'.$googleId.'/trust',
            [
                'timeout' => $this->timeout_sec,
                'connect_timeout' => $this->connect_timeout_sec,
                'headers' => [
                    'Accept'        => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                    'User-Agent'    => $this->userAgent,
                ],
            ]
        );
        return json_decode((string) $response->getBody(), true);
    }

    /**
     * Fetches Trust information for the user identified by the given $gtid.
     * The request is authenticated using the given AccessToken
     *
     * @param   string                      $tgid
     * @param   \RESWUE\Trust\AccessToken   $token
     * @return  mixed
     */
    public function getTrustInformationByTelegramId($tgid, AccessToken $token)
    {
        $response = $this->getHttpClient()->request(
            'GET',
            $this->url.'/api/v4/user/telegram:'.$tgid.'/trust',
            [
                'timeout' => $this->timeout_sec,
                'connect_timeout' => $this->connect_timeout_sec,
                'headers' => [
                    'Accept'        => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                    'User-Agent'    => $this->userAgent,
                ],
            ]
        );

        return json_decode((string) $response->getBody(), true);
    }

    /**
     * Fetches Trust information for the user identified by the given $trustmasterId.
     * The request is authenticated using the given AccessToken
     *
     * @param   string                      $rid
     * @param   \RESWUE\Trust\AccessToken   $token
     * @return  mixed
     */
    public function getTrustInformationByTrustmasterId($trustmasterId, AccessToken $token)
    {
        $response = $this->getHttpClient()->request(
            'GET',
            $this->url.'/api/v4/user/'.$trustmasterId.'/trust',
            [
                'timeout' => $this->timeout_sec,
                'connect_timeout' => $this->connect_timeout_sec,
                'headers' => [
                    'Accept'        => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                    'User-Agent'    => $this->userAgent,
                ],
            ]
        );

        return json_decode((string) $response->getBody(), true);
    }

    public function setProfileLink($trustmasterId, AccessToken $token, $link='')
    {
    if (!$trustmasterId) return(0);
	if ($link)
	{
        	$response = $this->getHttpClient()->request(
	            'PUT',
        	    $this->url.'/api/v4/user/'.$trustmasterId.'/external_profile',
	            [
                	'timeout' => $this->timeout_sec,
        	        'connect_timeout' => $this->connect_timeout_sec,
	                'headers' => [
                	    'Accept'        => 'application/json',
        	            'Authorization' => 'Bearer ' . $token,
	                    'User-Agent'    => $this->userAgent,
                	],
			'form_params' => array('external_profile' => $link),
            	]
        	);
	} else {
        	$response = $this->getHttpClient()->request(
	            'DELETE',
        	    $this->url.'/api/v4/user/'.$trustmasterId.'/external_profile',
	            [
                	'timeout' => $this->timeout_sec,
        	        'connect_timeout' => $this->connect_timeout_sec,
	                'headers' => [
                	    'Accept'        => 'application/json',
        	            'Authorization' => 'Bearer ' . $token,
	                    'User-Agent'    => $this->userAgent,
                	],
            	]
        	);
	}

	return json_decode((string) $response->getBody(), true);
    }

    public function getProfileLink($trustmasterId, AccessToken $token)
    {
        if ($link)
        {
                $response = $this->getHttpClient()->request(
                    'GET',
                    $this->url.'/api/v4/user/'.$trustmasterId.'/external_profile',
                    [
                        'timeout' => $this->timeout_sec,
                        'connect_timeout' => $this->connect_timeout_sec,
                        'headers' => [
                            'Accept'        => 'application/json',
                            'Authorization' => 'Bearer ' . $token,
                            'User-Agent'    => $this->userAgent,
                        ],
                ]
                );
        }
        return json_decode((string) $response->getBody(), true);
    }


    /**
     * Sets a custom User-Agent.
     *
     * @param  string $userAgent
     * @return $this
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    /**
     * Returns the current User-Agent.
     *
     * @return string
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * Get a instance of the Guzzle HTTP client.
     *
     * @return \GuzzleHttp\Client
     */
    protected function getHttpClient()
    {
        if (is_null($this->httpClient)) {
            $this->httpClient = new HttpClient();
        }

        return $this->httpClient;
    }

    /**
     * Set the Guzzle HTTP client instance.
     *
     * @param  \GuzzleHttp\Client  $client
     * @return $this
     */
    public function setHttpClient(HttpClient $client)
    {
        $this->httpClient = $client;

        return $this;
    }

    /**
     * @param string $uri
     */
    public function setRedirectUri($uri)
    {
        $this->redirectUri = $uri;
    }

    /**
     * @param array $scopes
     */
    public function setScopes(array $scopes)
    {
        $this->scopes = $scopes;
    }

    /**
     * Creates an authurl
     *
     * @return string
     */
    public function createAuthUrl()
    {
        $params = [
            'client_id' => $this->clientId,
            'redirect_uri' => $this->redirectUri,
            'response_type' => 'code',
            'scope' => implode(' ', $this->scopes),
            'state' => $this->state,
        ];
        $url = $this->url.'/oauth/authorize';

        return $url.'?'.http_build_query($params, '', '&');
    }

    /**
     * @param string $code
     * @return AccessToken
     */
    public function authenticate($code)
    {
        $response = $this->getHttpClient()->request(
            'POST',
            $this->url.'/oauth/token',
            [
                'timeout' => $this->timeout_sec,
                'connect_timeout' => $this->connect_timeout_sec,
                'form_params' => [
                    'grant_type'    => 'authorization_code',
                    'client_id'     => $this->clientId,
                    'client_secret' => $this->secret,
                    'scope'         => implode(' ', $this->scopes),
                    'code' => $code,
                    'redirect_uri' => $this->redirectUri,
                ],
                'headers' => [
                    'Accept'        => 'application/json',
                    'User-Agent'    => $this->userAgent,
                ],
            ]
        );

        return new AccessToken(json_decode((string) $response->getBody(), true));
    }

    /**
     * @param AccessToken $token
     * @return Profile
     */
    public function me(AccessToken $token)
    {
        $me = $this->call($token,'/me');
        $profile = new Profile($me);

        return $profile;
    }

    /**
     * @param AccessToken $token
     * @param string $path
     * @param array $params
     * @param string $method
     * @return array
     */
    private function call(AccessToken $token, $path, $params = [], $method = 'GET')
    {
        $uri = $this->url . '/api/v4' . $path;

        $response = $this->getHttpClient()->request(
            $method,
            $uri,
            [
                'timeout' => $this->timeout_sec,
                'connect_timeout' => $this->connect_timeout_sec,
                'headers' => [
                    'Accept'        => 'application/json',
                    'Authorization' => 'Bearer ' . $token->getToken(),
                    'User-Agent'    => $this->userAgent,
                ],
            ]
        );

        return json_decode((string) $response->getBody(), true);
    }
}
